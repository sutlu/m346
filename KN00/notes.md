# Structures of the Cloud AWS Semester 1 Module 2

## IaaS
Infrastructure as a service
 * physical and virtual computers
 * network features
 * storage

## PaaS
Platform as a Service
These are tools needed to manage underlying hardware and launch applications.
 * programming environements
 * application testing platforms
 * application launchers

## SaaS
Software as a Service
These are the actual Apps. You don't have to manage or install them you can jsut access and use them.

## Questions
 1. Q: How does your computer get information from the internet? When you open a website, where does the website come from? Who provides the data? Use what you have learned about computer science and cloud computing in your answer.
 A: From a Server (most likeley virtual for scalability reasons). 
 2. Q: What is a program or an app that you use that runs entirely in the cloud, meaning you don’t have to store anything on your computer or device? What do you use the program to do? How do you think the program is provided to you at little or no cost?
 A: 
 3. Q: More and more programs and apps are being moved from being stored locally on individual computers to being in the cloud. For example, many people now use internet-based word processing instead of software such as Microsoft Word, and Spotify instead of CDs and MP3 players. What is another program or service that you think will move into the cloud? Why do you think technology is moving in the direction of cloud computing? Give reasoning for your ideas based on what you have learned previously about cloud computing.
 A: 

## AWS Global Infrastructure
 * Region: 
   Areas where Data is stored
 * Availability Zone:
   Data Center. Each Region has multiple isolated locations known as Availability Zones. These Availability zones are connected with a low-latency link. They are represented by a region code and a letter identifyer: us-east-1a
 * Edge Location:
   A site where data can be stored for lower latency. Often, edge locations will be close to high-population areas that will generate high-traffic volumes.

# Create Graph

## Module 4
##### Amazon EC2:
A web service that provides secure, resizable compute capacity in the cloud. Think of it as renting a computer in the cloud. 

##### Amazon S3:
A service provided by Amazon Web Services (AWS) that stores data for users in the cloud.

##### DNS:
A naming system for computers, devices, and resources connected to a network. It binds IP addresses to domain names.

##### S3 bucket:
A container of objects (such as images, audio files, video files, documents, and so on) in Amazon S3.

##### Policy:
An object in AWS that, when associated with an identity or a resource, defines its permissions. AWS evaluates these policies when a principal entity (user or role) makes a request.

##### Domain name:
A label that identifies a network of computers under centralized control.

##### Amazone Route 53:
The AWS DNS web service.

##### Virtual private cloud (VPC)
A virtual network dedicated to your AWS account. It is logically isolated from other virtual networks in the AWS Cloud. All your AWS services can be launched from a VPC. It is useful for protecting your data and managing who can access your network.

##### JavaScript Object Notation (JSON)
A syntax for storing and exchanging data.

