## KN03

[TOC]

### A) Cloud-init Dateien Verstehen
Siehe [cloud-init.yaml](https://gitlab.com/sutlu/m346/-/blob/59edc2a7db40b0c85ea6a3f1dfa7bd55ac0c19ec/KN03/A/cloud-init.yaml)

### B) SSH-Key und Cloud-init
Angepasstes [cloud-init.yaml](https://gitlab.com/sutlu/m346/-/blob/59edc2a7db40b0c85ea6a3f1dfa7bd55ac0c19ec/KN03/B/cloud-init-KN03-B.yaml).

Details Instanz:
![details instance](B/instancekeypairatlaunch.png)

Versuch mit erstem Key:
![try first key](B/wihtluca-1.png)

Versuch mit zweitem Key:
![try second key](B/withluca-2.png)

Auszug Cloud-Init-Log:
![cloud init log](B/cloud-init-output.png)

### C) Template

Siehe [cloud-init-KN03-c.yaml](https://gitlab.com/sutlu/m346/-/blob/59edc2a7db40b0c85ea6a3f1dfa7bd55ac0c19ec/KN03/C/cloud-init-KN03-C.yaml)

### D) Auftrennung von Web- und Datenbankserver

Datenbank funktioniert:
![mariadbrunning](D/mariadbrunnig.png)

Zugriff vom Lokalen System auf Datenbank:
![connectionwithtelnet](D/connectionwithtelnet.png)

Cloud-init Datei vom db-Server: [cloud-init-db](https://gitlab.com/sutlu/m346/-/blob/59edc2a7db40b0c85ea6a3f1dfa7bd55ac0c19ec/KN03/D/cloud-init-db.yaml)

Webseiten:
![index.html](D/index_html.png)
![info.php](D/info_php.png)
![db.php](D/db_php.png)

Adminer:
![adminer](D/adminer.png)

Cloud-init Datei vom Web-Server: [cloud-init-web](https://gitlab.com/sutlu/m346/-/blob/59edc2a7db40b0c85ea6a3f1dfa7bd55ac0c19ec/KN03/D/cloud-init-web.yaml)

