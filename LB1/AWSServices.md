# AWS Services

### Compute

##### 1. Amazon EC2 *IaaS*
   1. Virtual Servers in the Cloud
   2. EC2 Auto Scaling, Scale compute capacity to meet demand
##### 2. Amazon Elastic Container Services / Elastic Kubenretes Services *CaaS*
   1. secure, reliable and scalable way to run containers
   2. trusted way to run Kubernetes
##### 3. AWS Lambda *Faas*
   1. Run code without thinking about servers

### Storage

##### 1. Amazon Elastic Block Store EBS
   1. EC2 block storage volumes
##### 2. Amazon Simple Storage Service S3/ S3 Glacier
   1. Object storage built to retrieve any amount of data from anywhere
   2. Low-cost archive storage in the cloud

### Database

##### 1. Amazon Aurora
   1. High performance managed relational database
##### 2. Amazon RDS
   1. Managed relational database service for MySQL, PostgreSQL, Oracle, SQL Server and Maria DB
##### 3. Amazon ElastiCache
   1. In-memory caching service
##### 4. Amazon DynamoDB
   1. Managed NoSQL database

