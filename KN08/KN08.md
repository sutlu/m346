## KN08

### A) Backup-Skript

Instanzen mit Tag:
![instanz1mittag](instanz1tag.png)

![Instanz2mittag](instanz2tag.png)

Liste der Snapshots:
![Snapshots](Snapshots.png)

Tags der erstellten Snapshots:
![tagdersnapshots](tagmitsnapshot.png)

Liste der Snapshots nach Cleanup:
![snapshotsnachcleanup](snapshotsnachcleanup.png)

### B) CRON-Job

Um die Lambda functions auszuführen kann man einfach einen Trigger hinzufügen. In unserem Fall ist das ein EventBridge (CloudWatch Events) Trigger.

![cloudwatchevent](cloudwatchevent.png)

Die EventBridge wird wie folgt konfiguriert:

![croncwconfig](croncwconfig.png)

In diesem Beispiel wird die Lambda Funktion täglich um 05:00 ausgeführt.

![cronjobcloudwatch](cronjobcloudwatch.png)