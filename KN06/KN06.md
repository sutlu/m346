## KN06

### A) Installation App
Ein Proxy ist ein Vermittler der zwischen einem Client und einem Server fungiert. Er leitet die Anfragen eines Clients an den Zielserver weiter und die Anfragen an den Client zurück.

Swagger:
![swagger](swagger.png)

Endpoint GetProducts:
![GetProducts](GetProducts.png)

MongoDB:
![MongoDB](Atlas.png)

### B) Vertikale Skalierung

##### Speicherskalierung
Speicherskalierung muss nicht heruntergefahren werden. Man wählt den EBS, der an die Instanz gekoppelt ist. Wählt dan oben rechts modify. Gibt die gewünschten Daten ein und speichert.

##### Instanztyp skalieren
Die Instanz muss herunter gefahren werden. Wenn die Instanz heruntergefahren ist, wählt man Action -> Instance settings -> change instance type. Dann wählt man den Instanz Typ der man will. Nun kann die VM wieder gestartet werden.

Für die Dokumentation der Skalierung verwende ich den Web-Server von KN05:

Vorher:

![Vorher](Vorher.png)

Nachher EBS skaliert:
![NachherEBS](NacherEBS.png)

Nachher Instanz Typ Skaliert:
![NacherType](nacherType.png)

### C) Horizontale Skalierung
Da ich keine Zeit mehr hatte den die Umgebung der Lehrperson zu zeigen versuche ich hier zu dokumentieren, wie ich den Load Balancer aufgesetzt habe. 

Man geht auf dem EC2 Dashboard auf LoadBalancer -> Create Load balancer

Wähle einen Application Load Balancer.

![ApplicationLoadBalancer](createAppLoadbalancer.png)

Gib dem Load Balancer einen Namen und wähle unter Network mapping zwei availability Zones mit den zutreffenden Subnets aus, in der deine Server Instanzen sind.

![loadbalanceconfig](LoadBalanceConfig.png)

Füge die Security Group der Server Instanzen hinzu.

![secrgroup](secrgroup.png)

Wähle nun unter "Listeners and routing" den link "create target group"

![listenersandrouting](listenerandrouting.png)

Füge unter "Health checks" den Pfad /Shop/Alive hinzu. So prüft die Target Group, ob beide Instanzen noch am laufen sind. Und drücke dann auf Next.

![targetgroupconfig2](targetgroupconfig2.png)

Wähle nun die Instanzen aus, welche für den Load Balancer versendet werden sollen und drücke auf " Include as pending below". Drücke dann auf "Create target group".

![chooseinstances](chooseinstances.png)

Gehe zurück zum Tab, in dem du den Load Balancer am kreieren bist. Wähle unter "Listener and routing" unter dem Punkt "Default action" in der Dropdown Liste die Target Group aus, die du zuvor konfiguriert hast. (falls die Target group nicht auftaucht kannst du die Dropdown liste refreshen). Drücke dann auf "Create Target Group".

Nun ist der Load balancer erstellt. Es dauert einen Moment alles provisioniert ist. Sobald beim Load balancer der Status auf Active ist und in der Target Group die beiden Instanzen als Healthy angegeben sind kannst du unter: [DNS-Name-des-load-balancers]/swagger/index.html die Swagger Seite aufrufen.

### D) Auto Scaling
Da ich keine Zeit mehr hatte den die Umgebung der Lehrperson zu zeigen versuche ich hier zu dokumentieren, wie ich die Auto Scaling Group aufgesetzt habe.

Gehe im EC2 Dashboard auf Auto Scaling Groups -> Create Auto Scaling group

Gib der Group einen namen und drücke dann auf den Link "Create a launch template"

![launchtemplate](launchtemplate.png)

Erstelle nun ein Template, wie du deinen Server aufgesetzt haben willst. Dies entspricht denselben Schritten, wie wenn du eine neue EC2 Instanz manuell erstellst. Wähle den Typ, das OS, die Security Groups und füge eine Cloud-init Datei hinzu. Wenn das Template erstellt ist, gehe zurück in den Tab, in dem der Auto Scaler aufgesetzt wird und wähle das soeben erstellte Template aus (refresh der Dropdown liste eventuell nötig). Drücke dann auf next.

Wähle nun unter Punkt Network, die Availability Zone und das Subnet welches du im Template den Instanzen vergibst. Drücke auf Next.

![instancelaunchoptions](instancelaunchoptions.png)

Unter den advanced options kann nun der Auto Scaler direkt einem Load balancer angehängt werden. So werden neue Instanzen direkt der angegebenen Target Group hinzugefügt. Drücke dann auf Next.

![advancedoptions](advancedoptions.png)

Unter Group size definieren wir wie viele Instanzen laufen sollen, wie viele minimum laufen müssen und wie viele maximal laufen dürfen.
Unter dem Punkt Scaling Policies kann nun festgelegt werden, wann eine neue Instanz generiert wird.
Wähle dazu den Punkt "Target tracking scaling policy" aus und stelle die Parameter nach eigenen Bedürfnissen ein.

![grsizescaleing](grsizescaling.png)

Prüfe nun deine Eingaben und bestätige am Schluss mit "Create Auto Scaling Group".

Nun steht der auto scaler. Die kann geprüft werden, indem eine der Instanzen die generiert wurden herunter fährst und somit eine Neue generiert wurde. Du erkennst die Instanzen daran, dass das Feld Name leer bleibt.